var http = require("http");
var fs = require("fs");
var host = '127.0.0.1';
var puerto = '9000';

var servidor = http.createServer(function (llamado, respuesta) {
    
    timestamp = Date();
    console.log(`[${timestamp}] ${llamado.method} -> ${llamado.url}`);
    
    if (llamado.url === '/') {
        fs.readFile("./index.html", "UTF-8", function (error, contenido) {
            if (error) {
                respuesta.writeHead(500, {'Content-type': "text/html"});
                respuesta.end('<h1>500 - Internal Server Error</h1>');
            } else {
                respuesta.writeHead(200, {'Content-type': "text/html"});
                respuesta.end(contenido);
            }
        });
    } else {
        respuesta.writeHead(404, {'Content-type': "text/html"});
        respuesta.end('<h1>404 - Not found</h1>');
    }

});

servidor.listen(puerto, host, function () {
    console.log(`El servidor está corriendo: ${host}:${puerto}`);
});

