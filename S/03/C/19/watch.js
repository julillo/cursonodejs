var fs = require('fs');

var config = JSON.parse(fs.readFileSync("archivos/config.json", "UTF-8"));

console.log("%j", config);

fs.watchFile("archivos/config.json", function (actual, anterior) {
    console.log("El archivo cambió");
    config = JSON.parse(fs.readFileSync("archivos/config.json", "UTF-8"));
    console.log("%j", config);
});