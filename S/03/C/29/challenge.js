var fs = require('fs');


var Challenge = {
    niveles: ["nivel1", "nivel2", "nivel3"],
    archivos: [],
    moverArchivo: function (nivel, nombreArchivo) {
        fs.rename(`/temp/${nombreArchivo}`, `${nivel}/${nombreArchivo}`, function (error) {
            if (error) {
                throw error;
            } else {
                console.log(`${nombreArchivo} -> ${nivel}/`);
            }
        });
    },
    moverArchivos: function () {
        Challenge.archivos.forEach(function (nombreArchivo) {
            if (nombreArchivo == '.DS_Store') {
                //borrar el archivo.
                fs.unlinkSync("temp/" + nombreArchivo);
                console.log("Eliminado '.DS_Store'");
            }

            if (nombreArchivo.indexOf("_1" === -1)) {
                Challenge.moverArchivo(Challenge.niveles[0], nombreArchivo);
            } else if (nombreArchivo.indexOf("_2" === -1)) {
                Challenge.moverArchivo(Challenge.niveles[1], nombreArchivo);
            } else if (nombreArchivo.indexOf("_3" === -1)) {
                Challenge.moverArchivo(Challenge.niveles[2], nombreArchivo);
            }
        });
    },
    borrarTempDir: function () {
        fs.rmdir("/temp", function (error) {
            if (error) {
                throw error;
            } else {
                console.log("'/temp' folder erased");
            }
        });
    },
    crearFolders: function () {
        fs.mkdir(Challenge.niveles[0]);
        fs.mkdir(Challenge.niveles[1]);
        fs.mkdir(Challenge.niveles[2]);
    },
    leerArchivos: function () {
        try {
            Challenge.archivos = fs.readdirSync("temp");
        } catch (error) {
            throw error;
        }
    },
    goChallenge: function () {

    }
};


//Challenge.leerArchivos();
//console.log(Challenge.archivos);

//Challenge.goChallenge();

//console.log(Challenge.niveles[0]);

Challenge.crearFolders();
Challenge.leerArchivos();
Challenge.moverArchivos();
Challenge.borrarTempDir();