var fs = require('fs');

var texto = "Aprende Node.JS Fácilmente";

fs.writeFile("archivo", texto, function (error) {
    if (error) {
        throw error;
    } else {
        console.log("El archivo fue creado");
    }
});

var nuevoTexto = "\nCurso por Jonathan Cifuentes";

fs.appendFile("./archivo", nuevoTexto, function(error){
    if(error){
        throw error;
    } else {
        console.log("El nuevo texto ha sido añadido al archivo.");
    }
});