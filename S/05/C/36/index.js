var express = require("express");
var app = express();
var port = 3000;

app.use("/virtual", express.static("public"));

app.get('/', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);
    respuesta.send("Hola desde Express");
});

app.listen(port, function () {
    console.log(`Aplicación corriendo en el puerto ${port}`);
});
