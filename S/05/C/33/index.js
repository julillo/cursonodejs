var express = require("express");
var app = express();
var port = 3000;

app.get('/', function (llamado, respuesta) {
    timestamp = Date();
    console.log(`[${timestamp}] ${llamado.method} -> ${llamado.url}`);
    respuesta.send("Hola desde Express");
});

app.listen(port, function () {
    console.log(`Aplicación corriendo en el puerto ${port}`);
});
