var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var port = 3000;

app.use(bodyParser.urlencoded({extended: false}));

app.get('/', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);
    respuesta.send("Hola desde Express");
});

app.get('/form', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);
    respuesta.sendFile(__dirname + "/" + "form.html");
});

app.post('/recibirPost', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);

    var nombre = llamado.body.nombre;
    var apellido = llamado.body.apellido;

    respuesta.send(`El nombre es: ${nombre}<br />El apellido es: ${apellido}`);
});

app.get('/get', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);

    var nombre = llamado.query.nombre;
    var apellido = llamado.query.apellido;

    respuesta.send(`El nombre es: ${nombre}<br />El apellido es: ${apellido}`);
});

app.listen(port, function () {
    console.log(`Aplicación corriendo en el puerto ${port}`);
});
