var conversion = require('./conversion');

var pulgadas = 3;
var millas = 5;

console.log(
        "%d pulgadas son %d metros",
        pulgadas,
        conversion.pulgadasaMetros(pulgadas)
        );

console.log(
        "%d millas son %d metros",
        millas,
        conversion.millasaMetros(millas)
        );