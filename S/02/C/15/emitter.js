var events = require("events");

var EventEmitter = events.EventEmitter;

var ee = new EventEmitter();

ee.once("cansado", function () {
    console.log(`El jugador está cansado por primera vez.`);
});

ee.on("cansado", function (data) {
    console.log(`El jugador está cansado. El jugador es el número ${data}.`);
});

ee.on("herido", function () {
    console.log(`El jugador está herido.`);
});

ee.emit("cansado", "10");
ee.emit("cansado", "10");
//ee.removeAllListeners("cansado");
ee.removeAllListeners();
ee.emit("cansado", "10");
ee.emit("herido");

