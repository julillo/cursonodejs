// ============================================================================ 
//  Console, objeto global.
// ============================================================================ 

console.log("Hola!");

console.log(2 + 3 + 4);

var numero1 = 5;

var numero2 = 10;

console.log(numero1 + numero2);

console.log(numero1 + " + " + numero2 + " = " + (numero1 + numero2));

console.log("%d + %d = %d", numero1, numero2, numero1 + numero2);

var json = {nombre: 'Juan', apellido: 'Lillo'};

console.log(json);

console.log("%j", json);

var nombre = "Juan Lillo";

console.log("%s", nombre);

var numero = 10;

console.log(`El nombre es: ${nombre} y el numero es: ${numero} `);

console.log(__filename);

console.log(__dirname);