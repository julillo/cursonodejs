// ============================================================================
//  Process, variable global
//  
//  como el main de c o java
// ============================================================================

console.log(process.argv);

function leerOpcion(opcion) {
    var index = process.argv.indexOf(opcion);
    if (index === -1) {
        return false;
    } else {
        return process.argv[index + 1];
    }
}

var nombre = leerOpcion("-nombre");

console.log("El nombre es: %s", nombre);